package com.dynamic.config;

import com.dynamic.config.controller.ApplicationConfigController;
import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.mapper.ApplicationConfigMapper;
import com.dynamic.config.resource.ApplicationConfigResource;
import com.dynamic.config.service.ApplicationConfigService;
import com.dynamic.config.type.ConfigurationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(ApplicationConfigController.class)
@ContextConfiguration(classes=ConfigApplication.class)
public class ConfigApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	ApplicationConfigService applicationConfigService;

	@MockBean
	ApplicationConfigMapper applicationConfigMapper;

	private final String URL = "/applicationConfig";

	@Test
	public void testAddEmployee() throws Exception {
		// prepare data and mock's behaviour
		ApplicationConfig applicationConfigStub = new ApplicationConfig();
		applicationConfigStub.setValue("trendyol.com");
		applicationConfigStub.setType(ConfigurationType.STRING);
		applicationConfigStub.setName("site");
		applicationConfigStub.setActive(true);
		applicationConfigStub.setApplicationName("SERVICE_A");
		when(applicationConfigService.save(any(ApplicationConfig.class))).thenReturn(applicationConfigStub);

		ApplicationConfigResource applicationConfigDTOStub = new ApplicationConfigResource();
		applicationConfigDTOStub.setIdentifier("dsdsd");
		applicationConfigDTOStub.setValue("trendyol.com");
		applicationConfigDTOStub.setType(ConfigurationType.STRING);
		applicationConfigDTOStub.setName("site");
		applicationConfigDTOStub.setActive(true);
		applicationConfigDTOStub.setApplicationName("SERVICE_A");

		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(URL).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(TestUtils.objectToJson(applicationConfigDTOStub))).andReturn();
		// verify
		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.CREATED.value(), status);

	}

	@Test
	public void testGetApplicationConfig() throws Exception {

		// prepare data and mock's behaviour
		ApplicationConfig applicationConfigStub = new ApplicationConfig();

		applicationConfigStub.setValue("trendyol.com");
		applicationConfigStub.setType(ConfigurationType.STRING);
		applicationConfigStub.setName("site");
		applicationConfigStub.setActive(true);
		applicationConfigStub.setApplicationName("SERVICE_A");
		when(applicationConfigService.findValueByKeyAndApplicationName("site", "SERVICE_A")).thenReturn(applicationConfigStub);
		;

		// execute
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.get(URL + "/{applicationName}/{key}", "one", "two")
						.accept(MediaType.APPLICATION_JSON_UTF8)).andReturn();

		// verify
		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);
	}

}






