package com.dynamic.config.library;

import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.service.ApplicationConfigService;
import com.dynamic.config.service.DatabaseDrivenMessageSource;
import com.dynamic.config.util.ApplicationConfigUtils;
import com.dynamic.config.util.ApplicationContextHolder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

import javax.sql.DataSource;
import java.util.concurrent.ExecutionException;

@ComponentScan("com.dynamic")
public class ConfigurationReader<T> {

    private String applicationName;
    private String connectionString;
    private Integer refreshTimerInternal;

    ApplicationConfigService applicationConfigService;

    DatabaseDrivenMessageSource databaseDrivenMessageSource;

    public ConfigurationReader(String applicationName,
                               String connectionString,
                               Integer refreshTimerInternal) {
        this.applicationName = applicationName;
        this.connectionString = connectionString;
        this.refreshTimerInternal = refreshTimerInternal;
        applicationConfigService = ApplicationContextHolder.getContext().
                getBean(ApplicationConfigService.class);
        databaseDrivenMessageSource = ApplicationContextHolder.getContext().
                getBean(DatabaseDrivenMessageSource.class);
        databaseDrivenMessageSource.setApplicationConfigService(applicationConfigService);
        databaseDrivenMessageSource.setApplicationKey(applicationName);
        databaseDrivenMessageSource.setRefreshTimerInternal(refreshTimerInternal);
        databaseDrivenMessageSource.buildCache();
    }

    public T getValue(String key, Class<T> type) throws ExecutionException {
        ApplicationConfig applicationConfig = ApplicationConfigUtils.findApplicationConfigValue(key);
         return type.cast(applicationConfig.getValue());
    }

    @Bean
    @Scope("prototype")
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(connectionString);
        dataSourceBuilder.username("");
        dataSourceBuilder.password("");
        return dataSourceBuilder.build();
    }

}
