package com.dynamic.config.resource;


import com.dynamic.config.type.ConfigurationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@ToString
public class ApplicationConfigResource  {
    private String identifier;
    private String name;
    private ConfigurationType type;
    private String value;
    private Boolean isActive;
    private String applicationName;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConfigurationType getType() {
        return type;
    }

    public void setType(ConfigurationType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof ApplicationConfigResource)) return false;

        ApplicationConfigResource that = (ApplicationConfigResource) o;

        return new EqualsBuilder()
                .append(identifier, that.identifier)
                .append(name, that.name)
                .append(type, that.type)
                .append(value, that.value)
                .append(isActive, that.isActive)
                .append(applicationName, that.applicationName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(identifier)
                .append(name)
                .append(type)
                .append(value)
                .append(isActive)
                .append(applicationName)
                .toHashCode();
    }
}
