package com.dynamic.config.util;

import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.service.DatabaseDrivenMessageSource;

import java.util.concurrent.ExecutionException;

public class ApplicationConfigUtils{


    public static ApplicationConfig findApplicationConfigValue(String value) throws ExecutionException {
        DatabaseDrivenMessageSource messageSource = ApplicationContextHolder.getContext().getBean(DatabaseDrivenMessageSource.class);
        return messageSource.getText(value);
    }
}





