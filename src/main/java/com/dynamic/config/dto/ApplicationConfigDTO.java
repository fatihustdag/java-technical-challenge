package com.dynamic.config.dto;


import com.dynamic.config.type.ConfigurationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class ApplicationConfigDTO {

    private String identifier;
    private String name;
    private ConfigurationType type;
    private String value;
    private Boolean isActive;
    private String applicationName;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConfigurationType getType() {
        return type;
    }

    public void setType(ConfigurationType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
}
