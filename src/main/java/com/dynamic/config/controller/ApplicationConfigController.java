package com.dynamic.config.controller;

import com.dynamic.config.dto.ApplicationConfigDTO;
import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.mapper.ApplicationConfigMapper;
import com.dynamic.config.resource.ApplicationConfigResource;
import com.dynamic.config.service.ApplicationConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/applicationConfig",produces = "application/hal+json")
public class ApplicationConfigController {

    @Autowired
    private ApplicationConfigService applicationConfigService;

    @Autowired
    private ApplicationConfigMapper applicationConfigMapper;

    @GetMapping(value = "/all/{applicationName}")
    public ResponseEntity<List<ApplicationConfigResource>> findAllApplicationConfigsByApplicationName(@PathVariable final String applicationName) {
        final List<ApplicationConfigResource> collection =
                applicationConfigService.findAllApplicationConfigsByApplicationName(applicationName).stream()
                        .map(u -> applicationConfigMapper.toResource(u))
                        .collect(Collectors.toList());
        return new ResponseEntity<List<ApplicationConfigResource>>(collection, HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<ApplicationConfigResource>> findAllApplicationConfigs() {
        final List<ApplicationConfigResource> collection =
                applicationConfigService.findAllApplicationConfigs().stream()
                        .map(u -> applicationConfigMapper.toResource(u))
                        .collect(Collectors.toList());
        return new ResponseEntity<List<ApplicationConfigResource>>(collection, HttpStatus.OK);
    }

    @GetMapping(value = "/{identifier}")
    public ResponseEntity<ApplicationConfigResource> findApplicationConfigByIdentifier(@PathVariable final String identifier) {
        final ApplicationConfigResource applicationConfig=
                applicationConfigMapper.toResource(applicationConfigService.findApplicationConfigById(identifier));
        return new ResponseEntity<ApplicationConfigResource>(applicationConfig, HttpStatus.OK);
    }

    @GetMapping(value = "/{applicationName}/{name}")
    public ResponseEntity<ApplicationConfigResource> findApplicationConfigByNameAndApplicationName(@PathVariable final String applicationName,
                                                                               @PathVariable final String name) {
        return new ResponseEntity<ApplicationConfigResource>(applicationConfigMapper.toResource
                (applicationConfigService.findValueByKeyAndApplicationName(name,applicationName)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ApplicationConfigResource> save(@RequestBody ApplicationConfigDTO applicationConfigDTO) {
        ApplicationConfig applicationConfig = applicationConfigMapper.toEntity(applicationConfigDTO);
        applicationConfigService.save(applicationConfig);
        ApplicationConfigResource applicationConfigResource = applicationConfigMapper.toResource(applicationConfig);
        return new ResponseEntity<ApplicationConfigResource>(applicationConfigResource, HttpStatus.CREATED);
    }


}
