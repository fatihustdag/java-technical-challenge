package com.dynamic.config.entitiy;

import com.dynamic.config.constant.ApplicationConstant;
import com.dynamic.config.type.ConfigurationType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Table(name ="ApplicationConfigs")
@Entity
public class ApplicationConfig {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private String identifier;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private ConfigurationType type;

    @Column(nullable = false)
    private String value;

    @Column(nullable = false)
    private Boolean isActive;

    @Column(nullable = false)
    private String applicationName;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConfigurationType getType() {
        return type;
    }

    public void setType(ConfigurationType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public String toString() {
        return "ApplicationConfig{" +
                "identifier='" + identifier + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", value='" + value + '\'' +
                ", isActive=" + isActive +
                ", applicationName='" + applicationName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof ApplicationConfig)) return false;

        ApplicationConfig that = (ApplicationConfig) o;

        return new EqualsBuilder()
                .append(getIdentifier(), that.getIdentifier())
                .append(getName(), that.getName())
                .append(getType(), that.getType())
                .append(getValue(), that.getValue())
                .append(isActive, that.isActive)
                .append(getApplicationName(), that.getApplicationName())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getIdentifier())
                .append(getName())
                .append(getType())
                .append(getValue())
                .append(isActive)
                .append(getApplicationName())
                .toHashCode();
    }
}
