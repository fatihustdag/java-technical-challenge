package com.dynamic.config;

import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.library.ConfigurationReader;
import com.dynamic.config.service.ApplicationConfigService;
import com.dynamic.config.type.ConfigurationType;
import com.dynamic.config.util.ApplicationContextHolder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
@Configuration
@EnableCaching
public class ConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigApplication.class, args);
		ConfigurationReader configurationReader = new ConfigurationReader("SERVICE_A","",1);
		ApplicationConfigService applicationConfigService  = ApplicationContextHolder.getContext().
				getBean(ApplicationConfigService.class);
		ApplicationConfig applicationConfig = new ApplicationConfig();
		applicationConfig.setActive(true);
		applicationConfig.setApplicationName("SERVICE_A");
		applicationConfig.setName("site");
		applicationConfig.setType(ConfigurationType.STRING);
		applicationConfig.setValue("trendyol.com");
		applicationConfigService.save(applicationConfig);
		try {
			String value = (String) configurationReader.getValue("site", String.class);
			System.out.println(value + "Value Found");
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}



}
