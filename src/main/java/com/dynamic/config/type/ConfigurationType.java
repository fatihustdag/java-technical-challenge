package com.dynamic.config.type;

public enum ConfigurationType {
    DOUBLE,
    BOOLEAN,
    STRING;
}
