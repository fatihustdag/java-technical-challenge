package com.dynamic.config.advice;

import com.dynamic.config.exception.ApplicationConfigNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class ExceptionHandler extends BaseExceptionHandler {

    public ExceptionHandler() {
        super();
    registerMapping(ApplicationConfigNotFoundException.class, "APPLICATION_CONFIG_NOT_FOUND","NO DATA FOUND ",NOT_FOUND);
}

}