package com.dynamic.config.advice;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

public abstract class BaseExceptionHandler extends ResponseStatusExceptionResolver {


    private static final ExceptionMapping DEFAULT_ERROR = new ExceptionMapping(
            "Internal Server Error",
            "SERVER_ERROR",
            INTERNAL_SERVER_ERROR);

    private final Map<Class, ExceptionMapping> exceptionMappings = new HashMap<>();

    public BaseExceptionHandler() {

        registerMapping(
                MissingServletRequestParameterException.class,
                "MISSING_PARAMETER",
                "Missing request parameter",
                BAD_REQUEST);
        registerMapping(
                MethodArgumentTypeMismatchException.class,
                "ARGUMENT_TYPE_MISMATCH",
                "Argument type mismatch",
                BAD_REQUEST);
        registerMapping(
                HttpRequestMethodNotSupportedException.class,
                "METHOD_NOT_SUPPORTED",
                "HTTP method not supported",
                METHOD_NOT_ALLOWED);
        registerMapping(
                ServletRequestBindingException.class,
                "MISSING_HEADER",
                "Missing header in request",
                BAD_REQUEST);
        registerMapping(
                MethodArgumentNotValidException.class,
                "METHOD_ARGUMENT_NOT_VALID",
                "Not Valid Argument",
                BAD_REQUEST);
    }


    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public ErrorResponse handleThrowable(final Throwable ex, final HttpServletResponse response) {
        ExceptionMapping mapping = exceptionMappings.getOrDefault(ex.getClass(), DEFAULT_ERROR);
        response.setStatus(mapping.status.value());
        return new ErrorResponse(mapping.code,mapping.message);
    }

    void registerMapping(
            final Class<?> clazz,
            final String code,
            final String message,
            final HttpStatus status) {
        exceptionMappings.put(clazz, new ExceptionMapping(code, message, status));
    }

    @Getter
    @Setter
    public static class ErrorResponse {
        private final String code;
        private final String message;

        ErrorResponse(String code, String message){
            this.code =code;
            this.message =message;
        }
    }

    private static class ExceptionMapping {
        private final String code;
        private final String message;
        private final HttpStatus status;

        ExceptionMapping(String code, String message, HttpStatus status){
            this.code =code;
            this.message =message;
            this.status = status;
        }

    }
}