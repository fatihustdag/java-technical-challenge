package com.dynamic.config.service;

import com.dynamic.config.entitiy.ApplicationConfig;

import java.util.List;

public interface ApplicationConfigService {
    ApplicationConfig findValueByKeyAndApplicationName(String key,String applicationName);
    List<ApplicationConfig> findAllApplicationConfigsByApplicationName(String applicationName);
    List<ApplicationConfig> findAllApplicationConfigs();
    ApplicationConfig save(ApplicationConfig applicationConfig);
    ApplicationConfig findApplicationConfigById(String id);
}
