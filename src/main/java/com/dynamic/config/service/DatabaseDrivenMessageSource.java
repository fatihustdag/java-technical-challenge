package com.dynamic.config.service;

import com.dynamic.config.entitiy.ApplicationConfig;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Component
public class DatabaseDrivenMessageSource {

    private String applicationKey;
    private LoadingCache<String, Map<String,ApplicationConfig>>  cache;
    private final static Integer BUCKET_SIZE = 200;
    private Integer refreshTimerInternal;

    @Autowired
    private ApplicationConfigService applicationConfigService;

    /*Cache loaded with filtering application Name and
    cache refresh time internal set by refreshTimerInterval value */
    public void buildCache(){
        cache = CacheBuilder.newBuilder().maximumSize(BUCKET_SIZE)
                .refreshAfterWrite(refreshTimerInternal, TimeUnit.MINUTES)
                .build(new CacheLoader<String,Map<String,ApplicationConfig>>() {
                    @Override
                    public Map<String,ApplicationConfig> load(String key) throws Exception {
                        Map<String, ApplicationConfig> m = new HashMap<>();
                        List<ApplicationConfig> texts = applicationConfigService.
                                findAllApplicationConfigsByApplicationName(applicationKey);
                        for (ApplicationConfig text : texts) {
                            m.put(text.getName(), text);
                        }
                        return m;
                    }
                });
    }

    public void setApplicationConfigService(ApplicationConfigService applicationConfigService) {
        this.applicationConfigService = applicationConfigService;
    }

    public void setApplicationKey(String applicationKey) {
        this.applicationKey = applicationKey;
    }

    public void setRefreshTimerInternal(Integer refreshTimerInternal) {
        this.refreshTimerInternal = refreshTimerInternal;
    }

    //Get application config from cache
    public ApplicationConfig getText(String code) throws ExecutionException {
        return cache.get("cacheAppConfig").get(code);
    }

    //Get All Configs from cache
    public Map<String,ApplicationConfig> getAll() throws ExecutionException {
        return cache.get("cacheAppConfig");
     }

}