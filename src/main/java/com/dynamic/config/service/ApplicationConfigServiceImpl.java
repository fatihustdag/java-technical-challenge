package com.dynamic.config.service;


import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.exception.ApplicationConfigNotFoundException;
import com.dynamic.config.repository.ApplicationConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ApplicationConfigServiceImpl implements ApplicationConfigService{

    @Autowired
    ApplicationConfigRepository applicationConfigRepository;

    public ApplicationConfig findValueByKeyAndApplicationName(String key, String applicationName) {
        Optional<ApplicationConfig> applicationConfig = applicationConfigRepository.
                findApplicationConfigByNameAndApplicationName(key,applicationName);
        applicationConfig.orElseThrow(()->new ApplicationConfigNotFoundException("applicationName:" +key));
        return applicationConfig.get();
    }

    public List<ApplicationConfig> findAllApplicationConfigsByApplicationName(String applicationName){
        return applicationConfigRepository.findAllByApplicationNameAndIsActiveTrue(applicationName).
                collect(Collectors.toList());
    }

    public List<ApplicationConfig> findAllApplicationConfigs(){
        return applicationConfigRepository.findAll().stream()
                .collect(Collectors.toList());
    }

    public ApplicationConfig save(ApplicationConfig applicationConfig) {
        return applicationConfigRepository.save(applicationConfig);
    }

    @Override
    public ApplicationConfig findApplicationConfigById(String identifier) {
        Optional<ApplicationConfig> applicationConfig = applicationConfigRepository.
                findApplicationConfigByIdentifier(identifier);
        applicationConfig.orElseThrow(()->new ApplicationConfigNotFoundException("Identifier:" +identifier));
        return applicationConfig.get();
    }
}
