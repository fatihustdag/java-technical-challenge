package com.dynamic.config.constant;

public class ApplicationConstant {

    public static final int IDENTIFIER_LENGTH = 36;

    private ApplicationConstant() {
        new AssertionError();
    }
}
