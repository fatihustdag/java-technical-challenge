package com.dynamic.config.repository;

import com.dynamic.config.entitiy.ApplicationConfig;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface ApplicationConfigRepository extends BaseRepository<ApplicationConfig> {
    Stream<ApplicationConfig> findAllByApplicationNameAndIsActiveTrue(String applicationName);
    Optional<ApplicationConfig> findApplicationConfigByNameAndApplicationName(String name, String applicationName);
    Optional<ApplicationConfig> findApplicationConfigByIdentifier(String identifier);
}


