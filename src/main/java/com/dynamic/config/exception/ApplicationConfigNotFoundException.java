package com.dynamic.config.exception;


public class ApplicationConfigNotFoundException extends BaseException{
    public ApplicationConfigNotFoundException(final String key) {
        super("Application ConfigurationReader could not be found with key: " + key);
    }
}
