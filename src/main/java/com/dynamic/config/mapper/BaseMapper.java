package com.dynamic.config.mapper;

public interface BaseMapper<DTO, Entity, Resource> {

    Entity toEntity(DTO dto);

    Resource toResource(Entity entity);
}
