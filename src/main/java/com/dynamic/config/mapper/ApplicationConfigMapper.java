package com.dynamic.config.mapper;

import com.dynamic.config.dto.ApplicationConfigDTO;
import com.dynamic.config.entitiy.ApplicationConfig;
import com.dynamic.config.resource.ApplicationConfigResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring" )
public interface ApplicationConfigMapper extends BaseMapper<ApplicationConfigDTO,ApplicationConfig,ApplicationConfigResource>{

}
